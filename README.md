# RSYNC the repository files to external server

1. Create new SSH-key, that the CI/CD can use (and is the only user for this key)
2. On your external server, add the public key `id_rsa.pub` contents to `~/.ssh/authorized_keys`
3. Go to your repository CI/CD settings, e.g.: https://gitlab.com/kirbo/update-sftp/-/settings/ci_cd
4. Under the "Variables", introduce new variables (see [GitLab_repository_settings_cicd_variables.png](./public/images/GitLab_repository_settings_cicd_variables.png)):
    1. Server address:
       ```
       key: host
       value: address.to.your.server.com
       [x] Protect variable
       [x] Mask variable
       [x] Expand variable reference
       ```
    2. Server username:
       ```
       key: user
       value: your_username
       [x] Protect variable
       [ ] Mask variable
       [x] Expand variable reference
       ```
    3. Path to what you want to rsync from this repository:
       ```
       key: source_path
       value: .
       [x] Protect variable
       [ ] Mask variable
       [x] Expand variable reference
       ```
       Use the value `.` if you want to upload _EVERYTHING_ from this repository into the remote target. If you only want the contents from `public` folder, then the `source_path` value should be `public/` and remote target path something like `~/www/project1`, then everything FROM `public` folder will be synced to `~/www/project1` folder. If you don't add the trailing `/` in the public, if will end up uploading to `~/www/project1/public`.
    4. Path where to sync the contents on the external server:
       ```
       key: external_target_path
       value: ~/www/project1
       [x] Protect variable
       [ ] Mask variable
       [x] Expand variable reference
       ````
    5. Private SSH-key to use on the CI/CD pipeline:
       ```
       key: ssh_key
       value: <contents of your `id_rsa` file>
       [x] Protect variable
       [ ] Mask variable
       [x] Expand variable reference
       ```
    6. Option for extra flags for rsync:
       ```
       key: extra_flags
       value: 
       [x] Protect variable
       [ ] Mask variable
       [x] Expand variable reference
       ```
       Once you are happy about everything (source and target paths) and everything is working etc, if you want rsync to automagically delete files from external server that were removed from this repository, add ` --delete-after` as the value of this variable.
5. Everything should be working now. Just make modifications to files and watch your external server contents be synced